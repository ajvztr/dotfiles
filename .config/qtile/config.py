# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import asyncio
import os
import subprocess
from typing import Callable, List
from libqtile.core.manager import Qtile

from libqtile import bar, layout, widget, qtile, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile import hook

# --- Variables ---
mod = "mod4"
terminal = guess_terminal()

# --- Theme material ---
# https://material-theme.com/docs/reference/color-palette/#material-themes
material_theme = dict(
    foreground='#B0BEC5',
    background='#212121',
    white='#eeffff',
    gray='#616161',
    green='#c3e88d',
    blue='#82aaff',
    yellow='#ffcb6b',
    orange='#FF9800',
    red='#f07178',
    purple='#c792ea',
    cyan='#89ddff',
    error='#ff5370',
    button='#474747',
    disabled='#2A2A2A',
)

# --- Keys ---
# https://docs.qtile.org/en/latest/manual/config/lazy.html
keys = [
    # Default keys
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(),
        desc="Move window up"),
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(),
        desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(),
        desc="Reset all window sizes"),
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
#    Key([mod], 'r', lazy.run_extension(extension.DmenuRun(
#        dmenu_prompt=">",
#        dmenu_bottom=False,
#        background=material_theme['background'],
#        foreground=material_theme['foreground'],
#        selected_background=material_theme['orange'],
#        selected_foreground=material_theme['button'],
#        font='Ubuntu',
#        fontsize=23,
#    ))),
#    Key([mod, "shift"], "r",
#        lazy.run_extension(
#            extension.WindowList(
#                dmenu_bottom=True,
#                font='Ubuntu',
#                fontsize=14,
#                )
#            )
#        ),
    Key([mod], "r", lazy.spawn('rofi -modi drun,run -show drun'),
        desc="Spawn a command using a prompt widget"),
    # Switch focus of monitors
    Key([mod], "period",
        lazy.next_screen(),
        desc='Move focus to next monitor'
        ),
    Key([mod], "comma",
        lazy.prev_screen(),
        desc='Move focus to prev monitor'
        ),
    # Fn keys on Thinkpad x1 carbon gen 6
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +5%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 5%-")),
    Key([], "XF86AudioRaiseVolume",
        lazy.spawn("amixer sset Master 5%+")),
    Key([], "XF86AudioLowerVolume",
        lazy.spawn("amixer sset Master 5%-")),
    Key([], "XF86AudioMute",
        lazy.spawn("amixer sset Master toggle")),
    Key([], "XF86AudioMicMute",
        lazy.spawn("amixer sset Capture toggle")),
]

# --- Groups ---
# http://docs.qtile.org/en/latest/manual/config/groups.html
groups: List[Group] = [
    Group('1', label=''),
    Group('2', label=''),
    Group('3', label=''),
    Group('8', label=''),
    Group('9', label=''),
    Group('0', label=''),
]


def _go_to_group(name: str) -> Callable:
    """
    This creates lazy functions that jump to a given group. When there is more than one
    screen, the first 3 and second 3 groups are kept on the first and second screen.
    E.g. going to the fourth group when the first group (and first screen) is focussed
    will also change the screen to the second screen.
    """
    def _inner(qtile: Qtile) -> None:
        if len(qtile.screens) == 1:
            qtile.groups_map[name].cmd_toscreen()
            return

        old = qtile.current_screen.group.name
        if name in '123':
            qtile.focus_screen(0)
            if old in '123' or qtile.current_screen.group.name != name:
                qtile.groups_map[name].cmd_toscreen()
        else:
            qtile.focus_screen(1)
            if old in '890' or qtile.current_screen.group.name != name:
                qtile.groups_map[name].cmd_toscreen()

    return _inner


# --- Keys for group ---
for i in groups:
    keys.append(Key([mod], i.name, lazy.function(_go_to_group(i.name))))
    keys.append(Key([mod, "shift"], i.name, lazy.window.togroup(i.name)))


# --- Groupbox widget ---
groupbox_config = {
    'active': material_theme['button'],
    'inactive': material_theme['white'],
    'fontsize': 48,
    'background': material_theme['background'],
    'foreground': material_theme['foreground'],
    'this_current_screen_border': material_theme['orange'],
    'this_screen_border': material_theme['foreground'],
    'other_current_screen_border': material_theme['orange'],
    'other_screen_border': material_theme['foreground'],
    'highlight_method': 'block'
}

groupboxes = [
    widget.GroupBox(**groupbox_config,
                    visible_groups=['1', '2', '3', '8', '9', '0']),
    widget.GroupBox(**groupbox_config, visible_groups=['8', '9', '0']),
]

# --- Layout ---
# http://docs.qtile.org/en/latest/manual/config/layouts.html
layouts = [
    layout.Columns(
        border_focus=[material_theme["orange"]],
        border_normal=[material_theme["background"]],
        border_width=4,
        margin=6),
    # layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)

# --- Screens and widget ---
widget_defaults = dict(
    font="Ubuntu",
    fontsize=24,
    padding=8,
)

extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Spacer(length=35),
                groupboxes[0],
                # widget.Spacer(length=35),
                # widget.TaskList(borderwidth=4,
                #                 rounded=False,
                #                 highlight_method="border",
                #                 margin=4,
                #                 border=material_theme['cyan']),
                widget.Spacer(),
                widget.Clipboard(),
                widget.Spacer(length=35),
                widget.Clipboard(),
                widget.Clock(format="%H:%M %A %d.%m:%Y"),
                widget.Spacer(length=35),
                widget.Systray(icon_size=32, padding=6),
                widget.Spacer(length=10),
                widget.ThermalSensor(),
                widget.Volume(fmt=" {}"),
                widget.Battery(format=' {percent:2.0%}'),
                # widget.Bluetooth(fmt=" {}", background=material_theme['red']),
                widget.Spacer(length=35),
                widget.QuickExit(default_text='', fontsize=48),
                widget.Spacer(length=35),
            ],
            48,
            background=material_theme['background'],
            margin=[6, 6, 0, 6],
            wallpaper='~/Pictures/wallpapers/rug.jpg'
        ),
    ),
    Screen(
        top=bar.Bar(
            [
                widget.Spacer(length=35),
                groupboxes[1],
                widget.Spacer(),
                widget.Clock(format="%H:%M %A %d.%m:%Y"),
                widget.Spacer(lenght=35),
                # widget.TaskList(borderwidth=4,
                #                 rounded=False,
                #                 highlight_method="border",
                #                 margin=4,
                #                 border=material_theme['cyan'],),
                # widget.Spacer(length=35),
            ],
            48,
            background=material_theme['background'],
            margin=[6, 6, 0, 6],
        ),
    ),
]

# --- Mouse ---
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

# --- Static variable ---
dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

auto_minimize = True
wl_input_rules = None

wmname = "LG3D"


# Hook. Example: https://docs.qtile.org/en/latest/manual/config/hooks.html
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])


@hook.subscribe.startup
def _():
    # Set up initial GroupBox visible groups
    if len(qtile.screens) > 1:
        qtile.groups_map['1'].cmd_toscreen(0, toggle=False)
        qtile.groups_map['0'].cmd_toscreen(1, toggle=False)

        groupboxes[0].visible_groups = ['1', '2', '3']
    else:
        groupboxes[0].visible_groups = ['1', '2', '3', '8', '9', '0']


@hook.subscribe.screen_change
async def _(_):
    # Reconfigure GroupBox visible groups
    await asyncio.sleep(1)  # Am I gonna fix this?
    if len(qtile.screens) > 1:
        if qtile.screens[0].group.name not in '123':
            qtile.groups_map['1'].cmd_toscreen(0, toggle=False)
        qtile.groups_map['0'].cmd_toscreen(1, toggle=False)

        groupboxes[0].visible_groups = ['1', '2', '3']
    else:
        groupboxes[0].visible_groups = ['1', '2', '3', '8', '9', '0']
    if hasattr(groupboxes[0], 'bar'):
        groupboxes[0].bar.draw()
